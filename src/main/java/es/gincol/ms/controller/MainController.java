package es.gincol.ms.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class MainController {

	@Value("${user.role}")
	String role = "User";
	
	@Value("${spring.datasource.username}")
	String username;
	
	@Value("${spring.datasource.password}")
	String password;
	
	@GetMapping(path = "/whoami/{username}")
	public String whoami(@PathVariable("username") String username) {
		return String.format("Hola! %s,  eres %s", username, role);
	}
	
	@GetMapping(path = "/database")
	public String database() {
		return String.format("el username es %s, y el password es %s",username, password);
	}

}
